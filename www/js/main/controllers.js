// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('searchtubeapp', ['ionic', 'ngSanitize', 'ngStorage', 'ngCordova', 'ngIOS9UIWebViewPatch', 'mobsocial.products']);
// not necessary for a web based app // needed for cordova/ phonegap application
app.run(function($ionicPlatform, $ionicHistory, $ionicPopup, $state) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (navigator && navigator.splashscreen)
            navigator.splashscreen.hide();
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
        if (window.StatusBar) {
            // Set the statusbar to use the default style, tweak this to
            // remove the status bar on iOS or change it to use white instead of dark colors.
            StatusBar.styleDefault();
        }
    });

    // Disable BACK button on home
    $ionicPlatform.registerBackButtonAction(function(event) {
      if (true) { // your check here
        $ionicPopup.confirm({
          title: 'System warning',
          template: 'are you sure you want to exit?'
        }).then(function(res) {
          if (res) {
            ionic.Platform.exitApp();
          }
        })
      }
    }, 100);

});
//app run getting device id
app.run(function($rootScope, myPushNotification) {
    // app device ready
    document.addEventListener("deviceready", function() {
        if (!localStorage.device_token_syt || localStorage.device_token_syt == '-1') {
            myPushNotification.registerPush();
        }
    });
    $rootScope.get_device_token = function() {
        if (localStorage.device_token_syt) {
            return localStorage.device_token_syt;
        } else {
            return '-1';
        }
    }
    $rootScope.set_device_token = function(token) {
        localStorage.device_token_syt = token;
        return localStorage.device_token_syt;
    }
});
//myservice device registration id to localstorage
app.service('myService', ['$http', function($http) {
    this.registerID = function(regID, platform) {
        //alert(regID);
        localStorage.device_token_syt = regID;
    }
}]);

// config to disable default ionic navbar back button text and setting a new icon
// logo in back button can be replaced from /templates/sidebar-menu.html file
app.config(function($ionicConfigProvider) {
        $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-back').previousTitleText(false);
    })
    // intro controller //
app.controller('IntroCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate', '$ionicSideMenuDelegate', '$ionicHistory', function($scope, $state, $ionicSlideBoxDelegate, $ionicSideMenuDelegate, $ionicHistory) {

        // Called to navigate to the main app
        $scope.next = function() {
            $ionicSlideBoxDelegate.next();
        };
        $scope.previous = function() {
            $ionicSlideBoxDelegate.previous();
        };
        // Called each time the slide changes
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;
        };

        $ionicSideMenuDelegate.canDragContent(false);

        // discard and move to homepage
        $scope.discardIntroPage = function() {
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: true
            });
            $state.go('app.vsearch');
        }
    }])
    /* main controller function */
app.controller('MainCtrl', ['$location', '$ionicPlatform', '$state', '$scope', '$localStorage', '$sessionStorage', '$http', '$ionicSideMenuDelegate', '$ionicHistory', '$ionicLoading', 'SubCategoryData', 'ItemToSearch',
        function($location, $ionicPlatform, $state, $scope, $localStorage, $sessionStorage, $http, $ionicSideMenuDelegate, $ionicHistory, $ionicLoading, SubCategoryData, ItemToSearch) {

            //console.log($localStorage.searchItems);

            // Toggle left function for app sidebar
            if ($localStorage.showLogout == false) {
                $scope.hideMe = $localStorage.showLogout;
            } else {
                $scope.hideMe = true;
            }
            //console.log($scope.hideMe);

            $scope.loginFormData = {};
            $scope.doLogin = function() {
                $ionicLoading.show({
                    noBackdrop: false,
                    template: '<p class=""><ion-spinner icon="crescent"/><h5>LOGGING-IN</h5></p>'
                });
                var req = {
                    method: "POST",
                    data: {
                        "username": $scope.loginFormData.username,
                        "password": $scope.loginFormData.password,
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/loginuser",
                    timeout: 15000
                }
                $http(req)
                    .then(function(response) {
                        //console.log(response);
                        if (response.data.status == 'ok') {
                            $scope.hideMe = $localStorage.showLogout = true;
                            var userinfo = {
                                username: $scope.loginFormData.username,
                                userid: response.data.userid
                            }
                            $localStorage.userInfoStorage = userinfo;
                            $ionicHistory.clearCache();
                            $ionicHistory.clearHistory();
                            $ionicHistory.nextViewOptions({
                                disableBack: true,
                                historyRoot: true
                            });
                            $state.go('app.vsearch');
                            $ionicLoading.hide();
                        } else {
                            $scope.loginFormData.username = null;
                            $scope.loginFormData.password = null;

                            $localStorage.showLogout = false;
                            $localStorage.userInfoStorage = [];
                            $ionicPopup.alert({
                                title: 'Authentication Denied',
                                template: '<h5>Either your password or username was not correct. Please enter authentication credentials again.</h5>'
                            });
                            $ionicLoading.hide();
                        }
                    }, function() {
                        $ionicLoading.hide();
                    });
            }
            $scope.logOut = function() {
                $ionicLoading.show({
                    noBackdrop: false,
                    template: '<p class=""><ion-spinner icon="crescent"/><h5>LOGGING-OUT</h5></p>'
                });
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({
                    disableBack: true,
                    historyRoot: true
                });
                $scope.hideMe = $localStorage.showLogout = false;
                $state.go('app.vsearch');
                $ionicLoading.hide();
            }

            $scope.toggleLeft = function() {
                $ionicSideMenuDelegate.toggleLeft();
            };
            // go back to previous page
            $scope.goBackOne = function() {
                    $ionicHistory.goBack();
                }
                // sharing plugin
            $scope.shareMain = function() {
                var title = "Download Smove For Android";
                var url = "https://play.google.com/store/apps/details?id=com.myspecialgames.swipe";
                window.plugins.socialsharing.share(title, 'Care to Share', null, url, function() {}, function(errormsg) {
                    alert(errormsg);
                })
            }
            $scope.shareArticle = function(title, url) {
                window.plugins.socialsharing.share(title, null, null, url)
            }
            $scope.openLinkArticle = function(url) {
                window.open(url, '_system');
            }
            $scope.items = "";
            $http.get("http://$scope.apiurl/parent_categories.json")
                .then(function(response) {
                    $scope.items = response.data;
                }, function() {

                });
            $scope.ItemToSearchContainer = function(categoryname) {
                ItemToSearch.dataItem = categoryname;
                if ($location.path() == "/app/blog") {
                    $state.reload();
                }
            }
            $scope.groups = [];
            $scope.parents = [];
            $http.get("http://$scope.apiurl/searchtube.json")
                .then(function(response) {
                    $scope.parents = response.data;
                    for (var k in response.data) {
                        $scope.groups[k] = {
                            name: response.data[k].category_name,
                            items: response.data[k].children
                        };
                        //console.log(response.data[k]);
                    }
                }, function() {

                });
            $scope.SearchItemForm = function() {
                    if (this.searchitem) {
                        $scope.searchitem = this.searchitem;
                        ItemToSearch.dataItem = $scope.searchitem;
                        this.searchitem = "";
                        $ionicSideMenuDelegate.toggleRight();
                        if ($location.path() == "/app/blog") {
                            $state.reload();
                        } else {
                            $state.go('app.blog');
                        }
                    }
                }
                /* toggle menu */
            $scope.toggleGroup = function(group) {
                if ($scope.isGroupShown(group)) {
                    $scope.shownGroup = null;
                } else {
                    $scope.shownGroup = group;
                }
            };
            $scope.isGroupShown = function(group) {
                return $scope.shownGroup === group;
            };
        }
    ])
    // subcategories page of app //
app.controller('SubCategoriesCtrl', ['$state', '$scope', '$http', '$filter', 'Features', 'SubCategoryData', 'ItemToSearch', '$ionicLoading', '$ionicPopup',
        function($state, $scope, $http, $filter, Features, SubCategoryData, ItemToSearch, $ionicLoading, $ionicPopup) {
            //alert("CategoryID is "+ SubCategoryData.dataObj);
            //$scope.items = Features.items;
            $scope.ItemToSearchContainer = function(categoryname) {
                ItemToSearch.dataItem = categoryname;
            }
            $ionicLoading.show({
                noBackdrop: false,
                template: '<p class=""><ion-spinner icon="lines"/><h5>LOADING</h5></p>'
            });
            $scope.subs = "";
            var req = {
                method: "POST",
                data: {
                    "subcategory": SubCategoryData.dataObj
                },
                url: "http://$scope.apiurl/assets/common/mobile/subcategories",
                timeout: 15000
            }
            $http(req).then(function(response) {
                //console.log(response);
                if (response.data <= 0) {
                    $state.go('app.vsearch');
                    $ionicPopup.alert({
                        title: 'Ooops! This is embarassing.',
                        template: '<h5> No sub-categories found for this parent category. Please try to choose another parent category again.</h5>'
                    });
                    $ionicLoading.hide();
                } else {
                    $scope.subs = response.data;
                    $ionicLoading.hide();
                }
            }, function(reject) {
                if (reject.status === 0) {
                    $ionicPopup.alert({
                        title: 'Connection Timeout',
                        template: '<h5>Request to server has timed out. Please try again.</h5>'
                    });
                    $ionicLoading.hide();
                    $state.go('app.vsearch');
                } else {
                    $ionicPopup.alert({
                        title: 'Error Occured',
                        template: '<h5>Please choose a parent category again to load search item options. [ErrorCode:' + reject.status + ']</h5>'
                    });
                    $ionicLoading.hide();
                    $state.go('app.vsearch');
                }
            });
        }
    ])
    /* Blog controller */
app.controller('WatchLaterCtrl', ['$scope', '$filter', '$state', '$localStorage', '$sessionStorage', '$ionicLoading', '$ionicPopup', '$ionicScrollDelegate', '$http', 'Blog', 'ChosenListingData', 'ItemToSearch',
        function($scope, $filter, $state, $localStorage, $sessionStorage, $ionicLoading, $ionicPopup, $ionicScrollDelegate, $http, Blog, ChosenListingData, ItemToSearch) {
            //console.log($localStorage.watchlaterlist);

            $scope.watchlaterfeeds = $localStorage.watchlaterlist;

            $scope.storeToServiceVariable = function(listingDataStore) {
                ChosenListingData.dataListing = listingDataStore;
                //console.log(ChosenListingData.dataListing);
            }

            $scope.removeThisListing = function(feedinfo) {
                var videoList = $localStorage.watchlaterlist;
                //console.log(feedinfo);
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove Feed',
                    template: '<div class="text-center">Feed will be removed on your list. Are you sure you want to remove this?<br><br><strong>' + $filter('decodeChars')(feedinfo.title) + '</strong></div>'
                });
                confirmPopup.then(function(res) {
                    if (res) {
                        $scope.temp_container = [];
                        for (var i = 0; i < videoList.length; i++) {
                            if (videoList[i].listingid != feedinfo.listingid) {
                                $scope.temp_container.push(videoList[i]);
                            }
                        }
                        $localStorage.watchlaterlist = $scope.temp_container;
                        $scope.watchlaterfeeds = $scope.temp_container;
                        $scope.temp_container = [];

                        //videoList.splice(indexNumber, 1);
                        //$localStorage.watchlaterlist = videoList;
                        //$scope.watchlaterfeeds = videoList;
                    }
                });
                $ionicScrollDelegate.scrollTop();
            }

        }
    ])
    // login page of app //
app.controller('LoginCtrl', ['$scope', '$filter', '$state', '$localStorage', '$sessionStorage', '$ionicLoading', '$ionicHistory', '$ionicScrollDelegate', '$ionicPopup', '$ionicModal', '$http', 'Blog', 'ChosenListingData', 'ItemToSearch',
        function($scope, $filter, $state, $localStorage, $sessionStorage, $ionicLoading, $ionicHistory, $ionicScrollDelegate, $ionicPopup, $ionicModal, $http, Blog, ChosenListingData, ItemToSearch) {
            // add your login logic here
            $ionicHistory.nextViewOptions({
                disableBack: true,
                historyRoot: true
            });


        }
    ])
    /* Blog controller */
app.controller('BlogCtrl', ['$scope', '$filter', '$state', '$localStorage', '$sessionStorage', '$ionicLoading', '$ionicScrollDelegate', '$ionicPopup', '$ionicModal', '$http', 'Blog', 'ChosenListingData', 'ItemToSearch',
        function($scope, $filter, $state, $localStorage, $sessionStorage, $ionicLoading, $ionicScrollDelegate, $ionicPopup, $ionicModal, $http, Blog, ChosenListingData, ItemToSearch) {

            if ($localStorage.globalsettings) {
                if ($localStorage.globalsettings.globalset == true) {
                    $scope.searchsettings = $localStorage.globalsettings;
                } else {
                    $scope.searchsettings = {
                        sortfield: "matchcount",
                        sortnetwork: "all",
                        sortdirection: "desc",
                        globalset: false
                    };
                }
            } else {
                $scope.searchsettings = {
                    sortfield: "matchcount",
                    sortnetwork: "all",
                    sortdirection: "desc",
                    globalset: false
                };
                //console.log($scope.searchsettings);
            }

            $scope.searchitem = '"' + ItemToSearch.dataItem + '"';
            $scope.feeds = [];
            $scope.postsCompleted = false;

            $scope.itempage = 1;

            $scope.getFeeds = function() {
                $ionicLoading.show({
                    noBackdrop: false,
                    template: '<p class=""><ion-spinner icon="lines"/><h5>LOADING</h5><sup>Please wait a moment</sup></p>'
                });
                var req = {
                    method: "POST",
                    data: {
                        "categoryname": encodeURIComponent(($filter('decodeChars')(ItemToSearch.dataItem)).replace(/[\|&;\$%@"<>\(\)\+,]/g, "")),
                        "page": $scope.itempage,
                        "sortdirection": $scope.searchsettings.sortdirection,
                        "sortfield": $scope.searchsettings.sortfield,
                        "sortnetwork": $scope.searchsettings.sortnetwork
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/searchresults",
                    timeout: 15000
                }
                $http(req)
                    .then(function(response) {
                        //console.log(JSON.parse(response.data.result));
                        //console.log($scope.itempage);
                        if (JSON.parse(response.data.result)) {
                            var hitcount = JSON.parse(response.data.result).searchinfo.hitcount;
                            var lastpage = JSON.parse(response.data.result).searchinfo.last;

                            if (lastpage < hitcount) {
                                $scope.itempage += 1;
                            } else {
                                $scope.postsCompleted = true;
                            }

                            $scope.feeds = $scope.feeds.concat(JSON.parse(response.data.result).listing);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $ionicLoading.hide();
                        } else {
                            $scope.postsCompleted = true;
                            $scope.feeds = null;
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to search for video feeds for this keyword. Please try again.</h5>'
                            });
                            $state.go('app.vsearch');
                        }

                    }, function(reject) {
                        $scope.feeds = null;
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to search for video feeds. Please try again. BLOG [ErrorCode:' + reject.status + ']</h5>'
                        });
                        $ionicLoading.hide();
                        $state.go('app.vsearch');
                    });
            }

            $scope.storeToServiceVariable = function(listingDataStore) {
                    //console.log(listingDataStore.field1);
                    if (listingDataStore.field1.length > 0) {
                        ChosenListingData.dataListing = listingDataStore;
                        $state.go('app.post');
                    } else {
                        var reqmain = {
                            method: "POST",
                            data: {
                                "hostname": 'searchtube.com',
                                "affiliate": 'SEARCHTUBE',
                                "source": listingDataStore.network,
                                "listingid": listingDataStore.listingid
                            },
                            url: "http://$scope.apiurl/assets/common/mobile/listingdetails",
                            timeout: 15000
                        }
                        $http(reqmain)
                            .then(function(response) {
                                //console.log(response.data.result);
                                if (response.data) {
                                    ChosenListingData.dataListing = response.data.result;
                                    $state.go('app.post');

                                    $scope.$broadcast('scroll.infiniteScrollComplete');
                                } else {
                                    $ionicPopup.alert({
                                        title: 'Error Occured',
                                        template: '<h5>An error occured while trying to load the video details. Please try again.</h5>'
                                    });
                                    $state.go('app.vsearch');
                                }

                            }, function(reject) {
                                $ionicPopup.alert({
                                    title: 'Error Occured',
                                    template: '<h5>An error occured while trying to search for video feeds. Please try again. BLOG [ErrorCode:' + reject.status + ']</h5>'
                                });
                            });
                    }
                    //console.log(listingDataStore);
                }
                // pull to refresh buttons
            $scope.doRefresh = function() {
                $scope.itempage = 1;
                $scope.feeds = [];
                $scope.postsCompleted = false;
                $scope.getFeeds();
                $scope.$broadcast('scroll.refreshComplete');
            }

            //Search Settings Modal
            $ionicModal.fromTemplateUrl('search-settings.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalsearchsettings = modal;
            });
            $scope.openModalSettings = function() {
                $scope.modalsearchsettings.show();
            };
            $scope.closeModalSettings = function() {
                $localStorage.globalsettings = [];
                $localStorage.globalsettings = $scope.searchsettings;

                $scope.itempage = 1;
                $scope.feeds = [];
                $scope.postsCompleted = false;
                $scope.getFeeds();
                $ionicScrollDelegate.scrollTop();

                $scope.modalsearchsettings.hide();
            };
        }
    ])
    /*   Articles controller  */
app.controller('PostCtrl', ['$scope', '$filter', '$http', '$sce', '$state', '$location', '$localStorage', '$sessionStorage', '$ionicPopup', '$cordovaInAppBrowser', '$ionicSideMenuDelegate', '$ionicLoading', '$ionicPopup', '$ionicScrollDelegate', 'Comments', 'ItemToSearch', '$ionicModal', 'PostData', 'ChosenListingData',
        function($scope, $filter, $http, $sce, $state, $location, $localStorage, $sessionStorage, $ionicPopup, $cordovaInAppBrowser, $ionicSideMenuDelegate, $ionicLoading, $ionicPopup, $ionicScrollDelegate, Comments, ItemToSearch, $ionicModal, PostData, ChosenListingData) {
            //console.log(ChosenListingData.dataListing);

            // Other Functions
            $scope.user = [];
            $scope.showme = null;
            $scope.showmetoo = null;
            $scope.user.Primary_Category = ChosenListingData.dataListing.primarycategory;
            $scope.user.Iweb_Category = "";
            $scope.user.categories =  ChosenListingData.dataListing.categories;
            $scope.user.contact_name = "";
            $scope.user.contact_email = "";
            $scope.user.contact_jobtitle = "";
            $scope.user.contact_phonenumber = "";
            $scope.user.contact_mobilenumber = "";
            $scope.user.company_name = $filter('htmlToPlaintext')(ChosenListingData.dataListing.network+'-'+ChosenListingData.dataListing.name);
            $scope.user.company_addressline1 = (ChosenListingData.dataListing.address != "") ? ChosenListingData.dataListing.address : ChosenListingData.dataListing.street;
            $scope.user.company_addressline2 = "";
            $scope.user.company_city = ChosenListingData.dataListing.city;
            $scope.user.company_state = ChosenListingData.dataListing.state;
            $scope.user.company_zip = ChosenListingData.dataListing.zip;
            $scope.user.company_country = 'United States',
            $scope.user.company_phonenumber = ChosenListingData.dataListing.phone;
            $scope.user.company_faxnumber = ChosenListingData.dataListing.extrafax;
            $scope.user.company_website = (ChosenListingData.dataListing.websiteurl != "") ? ChosenListingData.dataListing.websiteurl : ChosenListingData.dataListing.extrawebsiteurls;
            $scope.user.company_email = (ChosenListingData.dataListing.email != "") ? ChosenListingData.dataListing.email : ChosenListingData.dataListing.extraemails;
            $scope.user.company_videoready = "";
            $scope.user.company_verused = "";
            $scope.user.hosting = "";
            $scope.user.hostingsite = "";
            $scope.user.createvid = "";
            $scope.user.wehostvideo = "";

            $scope.showChoices = function() {
                if ($scope.user.company_videoready == 1) {
                    $scope.showme = true;
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                } else {
                    $scope.showme = false;
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                }
            }
            $scope.showHosting = function() {
                if ($scope.user.hosting == 'None') {
                    $scope.showmetoo = true;
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                } else if ($scope.user.hosting == 'Other') {
                    $scope.showmetoo = false;
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                } else {
                    $scope.showmetoo = null;
                    $ionicScrollDelegate.$getByHandle('mainScroll').scrollBottom();
                }
            }
            $scope.ownThisChannel = function() {
                $scope.userinfo = {
                    domainLive: 'searchtube.com',
                    company_website: $scope.user.company_website,
                    contact_name: $scope.user.contact_name,
                    contact_email: $scope.user.contact_email,
                    contact_jobtitle: $scope.user.contact_jobtitle,
                    contact_phonenumber: $scope.user.contact_phonenumber,
                    contact_mobilenumber: $scope.user.contact_mobilenumber,
                    company_name: $scope.user.company_name,
                    company_addressline1: $scope.user.company_addressline1,
                    company_addressline2: $scope.user.company_addressline2,
                    company_city: $scope.user.company_city,
                    company_state: $scope.user.company_state,
                    company_zip: $scope.user.company_zip,
                    company_country: 'United States',
                    company_phonenumber: $scope.user.company_phonenumber,
                    company_faxnumber: $scope.user.company_faxnumber,
                    company_website: $scope.user.company_website,
                    company_email: $scope.user.company_email,
                    company_videoready: $scope.user.company_videoready,
                    company_verused: $scope.user.company_verused,
                    Primary_Category: $scope.user.Primary_Category,
                    Iweb_Category: $scope.user.Iweb_Category,
                    hosting: $scope.user.hosting,
                    hostingsite: $scope.user.hostingsite,
                    createvid: $scope.user.createvid,
                    wehostvideo: $scope.user.wehostvideo,
                    categories: $scope.user.categories
                }
                //console.log(ChosenListingData);
                var owningreq = {
                    method: "POST",
                    data: $scope.userinfo,
                    url: "http://$scope.apiurl/assets/common/mobile/owncompanymobile",
                    timeout: 15000
                }
                $http(owningreq)
                    .then(function(response) {
                        //console.log(response);
                        var stresult = response.data;
                        if (stresult.trim() == "YES") {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Owning Successful',
                                template: '<h5>Channel owning was successful. Please check your email to verify the request of ownership of the Channel. Thank you.</h5>'
                            });
                            $scope.closeModal(5);
                        } else if (stresult.trim() == "FOUND") {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Similar Record',
                                template: '<h5>We found out that the channel you are trying to own has been owned by someone and request is under process. If this channel is yours please contact us to fix the ownership of the channel. Thank you!</h5>'
                            });
                            $scope.closeModal(5);
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured on our end while trying to own the channel. Please report it immediately. Below is the error.</h5><br>' + stresult
                            });
                            $scope.closeModal(5);
                        }

                    }, function(reject) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to own a Channel. Please restart the app then try owning process again. [Error#:' + reject.status + ']</h5>'
                        });
                        $scope.closeModal(5);
                    });
                //console.log($scope.userinfo);
            }

            //$scope.listing_videourl = $sce.trustAsResourceUrl(ChosenListingData.dataListing.videourl);
            if(ChosenListingData.dataListing.video != ""){
                $scope.listing_videourl = $sce.trustAsResourceUrl( 'http://dev.searchmercials.com/search' + ChosenListingData.dataListing.video );
            }
            if(ChosenListingData.dataListing.videourl != ""){
                $scope.listing_videourl = $sce.trustAsResourceUrl( ChosenListingData.dataListing.videourl );
            }

            $scope.listing_redirect = ChosenListingData.dataListing.listingid=='' ? $sce.trustAsResourceUrl('http://dev.searchmercials.com/search' + ChosenListingData.dataListing.redirect) : $sce.trustAsResourceUrl(ChosenListingData.dataListing.redirect);
            $scope.listing_title = $sce.trustAsHtml(ChosenListingData.dataListing.title);
            $scope.listing_description = $sce.trustAsHtml(ChosenListingData.dataListing.description);
            $scope.listing_channel = $sce.trustAsHtml(ChosenListingData.dataListing.field1);
            $scope.listing_site = ChosenListingData.dataListing.network.toUpperCase();


            $scope.getDetails = function() {
                $ionicLoading.show({
                    noBackdrop: false,
                    template: '<p class=""><ion-spinner icon="lines"/><h5>LOADING DETAILS</h5><sup>Please wait a moment</sup></p>'
                });
                var reqmain = {
                    method: "POST",
                    data: {
                        "hostname": 'searchtube.com',
                        "affiliate": 'SEARCHTUBE',
                        "source": ChosenListingData.dataListing.network,
                        "listingid": ChosenListingData.dataListing.listingid
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/listingdetails",
                    timeout: 15000
                }
                $http(reqmain)
                    .then(function(response) {
                        //console.log(response.data);

                        if (response.data) {
                            ChosenListingData.dataListing = response.data.result;
                            $scope.listing_videourl = $sce.trustAsResourceUrl(response.data.result.videourl);
                            $scope.listing_redirect = $sce.trustAsResourceUrl(response.data.result.redirect);
                            $scope.listing_title = $sce.trustAsHtml(response.data.result.title);
                            $scope.listing_description = $sce.trustAsHtml(response.data.result.description);
                            $scope.listing_channel = $sce.trustAsHtml(response.data.result.field1);
                            $scope.listing_site = response.data.network.toUpperCase();
                            $scope.user.company_name = ChosenListingData.dataListing.field1;
                            $ionicLoading.hide();
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to load the video information. Please try again.</h5>'
                            });
                            $state.go('app.blog');
                        }

                    }, function(reject) {
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to load the video feed. Please try again.  [Error#:' + reject.status + ']</h5>'
                        });
                        $state.go('app.blog');
                    });
            }

            $scope.shareMain = function() {
                var sharetitle = ChosenListingData.dataListing.title;
                var shareurl = isEmpty(ChosenListingData.dataListing.videourl) ? ('https://dev.searchmercials.com/search' + ChosenListingData.dataListing.video) : ChosenListingData.dataListing.videourl;
                var network = ChosenListingData.dataListing.network;
                var videoid = isEmpty(ChosenListingData.dataListing.listingid) ? ChosenListingData.dataListing.id : ChosenListingData.dataListing.listingid;
                var searchtubeurlshare = 'http://searchtube.com/video/' + videoid + '/' + network + '/';
                window.plugins.socialsharing.share('Cool video! Watch it now on SearchTube. - ', sharetitle, '', searchtubeurlshare, function() {}, function(errormsg) {
                    alert(errormsg);
                })
            }
            $scope.showToast = function() {

                $scope.foundin_list = false;
                if ($localStorage.watchlaterlist) {
                    var list = $localStorage.watchlaterlist;
                    for (var i = 0; i < list.length; i++) {
                        if (list[i].listingid == ChosenListingData.dataListing.listingid || list[i].listingid == ChosenListingData.dataListing.id) {
                            $scope.foundin_list = true;
                        }
                    }

                    if ($scope.foundin_list == true) {
                        $ionicPopup.alert({
                            title: 'Watch Later',
                            template: '<h5>Video is already on your playlist.</h5>'
                        });
                    } else {
                        $localStorage.watchlaterlist.push(ChosenListingData.dataListing);
                        $ionicPopup.alert({
                            title: 'Watch Later',
                            template: '<h5>Added to Watch Later playlist.</h5>'
                        });
                        /*window.plugins.toast.showWithOptions(
                          {
                            message: "Added to Watch Later playlist",
                            duration: "long", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
                            position: "top",
                            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
                          }
                        );*/
                    }
                } else {
                    $localStorage.watchlaterlist = [];
                    $localStorage.watchlaterlist.push(ChosenListingData.dataListing);
                    $ionicPopup.alert({
                        title: 'Watch Later',
                        template: '<h5>Added to Watch Later playlist.</h5>'
                    });
                }
            };

            $scope.openBrowser = function() {
                var options = {
                    location: 'yes',
                    clearcache: 'yes',
                    toolbar: 'yes',
                    closebuttoncaption: 'Back to Searchtube App'
                };
                var urlnagivate = /\b(http|https)/.test($scope.listing_redirect) ? $scope.listing_redirect : 'http://' + $scope.listing_redirect;
                $cordovaInAppBrowser.open(urlnagivate, '_blank', options)
                    .then(function(event) {}).catch(function(event) {});
            }

            $scope.activemodal = "";

            $scope.postsCompletedMoreLikeThis = true;
            $scope.networkMoreLikeThis = ChosenListingData.dataListing.network;
            $scope.pageMoreLikeThis = 1;
            $scope.feedsMoreLikeThis = [];

            $scope.moreLikeThis = function() {
                //console.log(($filter('decodeChars')(ChosenListingData.dataListing.title)).replace(/\W+/g, " "));
                var reqmorelikethis = {
                    method: "POST",
                    data: {
                        "hostname": 'searchtube.com',
                        "affiliate": 'SEARCHTUBE',
                        "categoryname": ($filter('decodeChars')(ChosenListingData.dataListing.title)).replace(/\W+/g, " "),
                        "page": $scope.pageMoreLikeThis,
                        "sortnetwork": null
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/more.like.this",
                    timeout: 15000
                }
                $http(reqmorelikethis)
                    .then(function(response) {
                        //console.log(JSON.parse(response.data.result));
                        //console.log(response);
                        if (JSON.parse(response.data.result)) {
                            var hitcount = JSON.parse(response.data.result).searchinfo.hitcount;
                            var lastpage = JSON.parse(response.data.result).searchinfo.last;

                            if (lastpage < hitcount) {
                                $scope.pageMoreLikeThis += 1;
                            } else {
                                $scope.postsCompletedMoreLikeThis = true;
                            }

                            $scope.feedsMoreLikeThis = $scope.feedsMoreLikeThis.concat(JSON.parse(response.data.result).listing);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $ionicLoading.hide();
                        } else {
                            $scope.postsCompletedMoreLikeThis = true;
                            $scope.feedsMoreLikeThis = null;
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to search for video feeds for this keyword. Please try again.</h5>'
                            });
                            //$state.go('app.vsearch');
                        }

                    }, function(reject) {
                        $scope.feedsMoreLikeThis = [];
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to search for video feeds. Please try again. MORE POST [ErrorCode:' + reject.statusText + ']</h5>'
                        });
                        $ionicLoading.hide();
                        //$state.go('app.vsearch');
                    });
            }

            $scope.postsCompletedMoreLikeThisNetwork = true;
            $scope.networkMoreLikeThisNetwork = ChosenListingData.dataListing.network;
            $scope.pageMoreLikeThisNetwork = 1;
            $scope.feedsMoreLikeThisNetwork = [];

            $scope.moreLikeThisNetwork = function() {
                //console.log(($filter('decodeChars')(ChosenListingData.dataListing.title)).replace(/\W+/g, " "));
                var reqmorelikethisnetwork = {
                    method: "POST",
                    data: {
                        "hostname": 'searchtube.com',
                        "affiliate": 'SEARCHTUBE',
                        "categoryname": ($filter('decodeChars')(ChosenListingData.dataListing.title)).replace(/\W+/g, " "),
                        "page": $scope.pageMoreLikeThisNetwork,
                        "network": $scope.networkMoreLikeThisNetwork
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/more.like.this",
                    timeout: 15000
                }
                $http(reqmorelikethisnetwork)
                    .then(function(response) {
                        //console.log(JSON.parse(response.data.result));
                        //console.log(response);
                        if (JSON.parse(response.data.result)) {
                            var hitcount = JSON.parse(response.data.result).searchinfo.hitcount;
                            var lastpage = JSON.parse(response.data.result).searchinfo.last;

                            if (lastpage < hitcount) {
                                $scope.pageMoreLikeThisNetwork += 1;
                            } else {
                                $scope.postsCompletedMoreLikeThisNetwork = true;
                            }

                            $scope.feedsMoreLikeThisNetwork = $scope.feedsMoreLikeThisNetwork.concat(JSON.parse(response.data.result).listing);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $ionicLoading.hide();
                        } else {
                            $scope.postsCompletedMoreLikeThisNetwork = true;
                            $scope.feedsMoreLikeThisNetwork = null;
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to search for video feeds for this keyword. Please try again.</h5>'
                            });
                            //$state.go('app.vsearch');
                        }

                    }, function(reject) {
                        $scope.feedsMoreLikeThisNetwork = [];
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to search for video feeds. Please try again. NETWORK POST [ErrorCode:' + reject.statusText + ']</h5>'
                        });
                        $ionicLoading.hide();
                        //$state.go('app.vsearch');
                    });
            }

            $scope.postsCompletedMoreLikeThisChannel = true;
            $scope.networkMoreLikeThisChannel = ChosenListingData.dataListing.network;
            $scope.channelMoreLikeThisChannel = ChosenListingData.dataListing.name;
            $scope.pageMoreLikeThisChannel = 1;
            $scope.feedsMoreLikeThisChannel = [];

            //console.log($scope.channelMoreLikeThisChannel);

            $scope.moreLikeThisChannel = function() {
                //console.log($scope.channelMoreLikeThisChannel);
                var reqmorelikethischannel = {
                    method: "POST",
                    data: {
                        "hostname": 'searchtube.com',
                        "affiliate": 'SEARCHTUBE',
                        "page": $scope.pageMoreLikeThisChannel,
                        "network": $scope.networkMoreLikeThisChannel,
                        "channel": $scope.channelMoreLikeThisChannel
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/more.from.channel",
                    timeout: 15000
                }
                $http(reqmorelikethischannel)
                    .then(function(response) {
                        //console.log(JSON.parse(response.data.result));
                        //console.log(response);
                        if (JSON.parse(response.data.result)) {
                            var hitcount = JSON.parse(response.data.result).searchinfo.hitcount;
                            var lastpage = JSON.parse(response.data.result).searchinfo.last;

                            if (lastpage < hitcount) {
                                $scope.pageMoreLikeThisChannel += 1;
                            } else {
                                $scope.postsCompletedMoreLikeThisChannel = true;
                            }

                            $scope.feedsMoreLikeThisChannel = $scope.feedsMoreLikeThisChannel.concat(JSON.parse(response.data.result).listing);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $ionicLoading.hide();
                        } else {
                            $scope.postsCompletedMoreLikeThisChannel = true;
                            $scope.feedsMoreLikeThisChannel = null;
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to search for video feeds for this keyword. Please try again.</h5>'
                            });
                            //$state.go('app.vsearch');
                        }

                    }, function(reject) {
                        $scope.feedsMoreLikeThisChannel = [];
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to search for video feeds. Please try again. CHANNEL POST [ErrorCode:' + reject.statusText + ']</h5>'
                        });
                        $ionicLoading.hide();
                        //$state.go('app.vsearch');
                    });
            }

            $scope.postsCompletedMoreLikeThisRecent = true;
            $scope.pageMoreLikeThisRecent = 1;
            $scope.feedsMoreLikeThisRecent = [];

            $scope.moreLikeThisRecent = function() {
                //console.log(($filter('decodeChars')(ChosenListingData.dataListing.title)).replace(/\W+/g, " "));
                var reqmorelikethisrecent = {
                    method: "POST",
                    data: {
                        "hostname": 'searchtube.com',
                        "affiliate": 'SEARCHTUBE',
                        "page": $scope.pageMoreLikeThisRecent,
                    },
                    url: "http://$scope.apiurl/assets/common/mobile/custom.listing",
                    timeout: 15000
                }
                $http(reqmorelikethisrecent)
                    .then(function(response) {
                        //console.log(JSON.parse(response.data.result));
                        //console.log(response);
                        if (JSON.parse(response.data.result)) {
                            var hitcount = JSON.parse(response.data.result).searchinfo.hitcount;
                            var lastpage = JSON.parse(response.data.result).searchinfo.last;

                            if (lastpage < hitcount) {
                                $scope.pageMoreLikeThisRecent += 1;
                            } else {
                                $scope.postsCompletedMoreLikeThisRecent = true;
                            }

                            $scope.feedsMoreLikeThisRecent = $scope.feedsMoreLikeThisRecent.concat(JSON.parse(response.data.result).listing);
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                            $ionicLoading.hide();
                        } else {
                            $scope.postsCompletedMoreLikeThisRecent = true;
                            $scope.feedsMoreLikeThisRecent = null;
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Error Occured',
                                template: '<h5>An error occured while trying to search for video feeds for this keyword. Please try again.</h5>'
                            });
                            //$state.go('app.vsearch');
                        }

                    }, function(reject) {
                        $scope.feedsMoreLikeThisRecent = [];
                        $ionicPopup.alert({
                            title: 'Error Occured',
                            template: '<h5>An error occured while trying to search for video feeds. Please try again. RECENT POST [ErrorCode:' + reject.statusText + ']</h5>'
                        });
                        $ionicLoading.hide();
                        //$state.go('app.vsearch');
                    });
            }

            $scope.storeToServiceVariable = function(listingDataStore) {
                if (listingDataStore.field1) {
                    ChosenListingData.dataListing = listingDataStore;
                    $scope.listing_videourl = $sce.trustAsResourceUrl(ChosenListingData.dataListing.videourl);
                    $scope.listing_redirect = $sce.trustAsResourceUrl(ChosenListingData.dataListing.redirect);
                    $scope.listing_title = $sce.trustAsHtml(ChosenListingData.dataListing.title);
                    $scope.listing_description = $sce.trustAsHtml(ChosenListingData.dataListing.description);
                    $scope.listing_channel = $sce.trustAsHtml(ChosenListingData.dataListing.field1);
                    $scope.listing_site = ChosenListingData.dataListing.network.toUpperCase();
                } else {
                    ChosenListingData.dataListing = listingDataStore;
                    $scope.getDetails();
                }
                $scope.closeModal(1);
                $scope.closeModal(2);
                $scope.closeModal(3);
                $scope.closeModal(4);
                $scope.doRefreshAll();

                $ionicScrollDelegate.scrollTop();
                //console.log(ChosenListingData.dataListing);
            }

            // pull to refresh buttons
            $scope.doRefreshAll = function() {
                $scope.pageMoreLikeThis = 1;
                $scope.feedsMoreLikeThis = [];
                //$scope.postsCompletedMoreLikeThis = false;
                //$scope.moreLikeThis();

                $scope.pageMoreLikeThisNetwork = 1;
                $scope.feedsMoreLikeThisNetwork = [];
                //$scope.postsCompletedMoreLikeThisNetwork = false;
                //$scope.moreLikeThisNetwork();

                $scope.pageMoreLikeThisChannel = 1;
                $scope.feedsMoreLikeThisChannel = [];
                //$scope.postsCompletedMoreLikeThisChannel = false;
                //$scope.moreLikeThisChannel();

                $scope.pageMoreLikeThisRecent = 1;
                $scope.feedsMoreLikeThisRecent = [];
                //$scope.postsCompletedMoreLikeThisRecent = false;
                //$scope.moreLikeThisRecent();

                $scope.$broadcast('scroll.refreshComplete');
            }

            // more network modal
            $ionicModal.fromTemplateUrl('more-allnetwork-modal.html', {
                id: 1,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalallnetwork = modal;
            });
            // more options modal
            $ionicModal.fromTemplateUrl('more-currentnetwork-modal.html', {
                id: 2,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalcurrentnetwork = modal;
            });
            // more channel modal
            $ionicModal.fromTemplateUrl('more-channel-modal.html', {
                id: 3,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalchannel = modal;
            });
            // more channel modal
            $ionicModal.fromTemplateUrl('more-recent-modal.html', {
                id: 4,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalrecent = modal;
            });
            // own network modal
            $ionicModal.fromTemplateUrl('own-channel-modal.html', {
                id: 5,
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modalowning = modal;
            });
            $scope.openModal = function(index) {
                switch (index) {
                    case 1:
                        $scope.postsCompletedMoreLikeThis = false;
                        $scope.modalallnetwork.show();
                        $scope.activemodal = "modalallnetwork";
                        break;
                    case 2:
                        $scope.postsCompletedMoreLikeThisNetwork = false;
                        $scope.modalcurrentnetwork.show();
                        $scope.activemodal = "modalcurrentnetwork";
                        break;
                    case 3:
                        $scope.postsCompletedMoreLikeThisChannel = false;
                        $scope.modalchannel.show();
                        $scope.activemodal = "modalchannel";
                        break;
                    case 4:
                        $scope.postsCompletedMoreLikeThisRecent = false;
                        $scope.modalrecent.show();
                        $scope.activemodal = "modalrecent";
                        break;
                    case 5:
                        $scope.modalowning.show();
                        $scope.activemodal = "modalowning";
                        break;
                }
            };
            $scope.closeModal = function(index) {
                switch (index) {
                    case 1:
                        $scope.modalallnetwork.hide();
                        $scope.postsCompletedMoreLikeThis = true;
                        break;
                    case 2:
                        $scope.modalcurrentnetwork.hide();
                        $scope.postsCompletedMoreLikeThisNetwork = true;
                        break;
                    case 3:
                        $scope.modalchannel.hide();
                        $scope.postsCompletedMoreLikeThisChannel = true;
                        break;
                    case 4:
                        $scope.modalrecent.hide();
                        $scope.postsCompletedMoreLikeThisRecent = true;
                        break;
                    case 5:
                        $scope.modalowning.hide();
                        break;
                }
            };
            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function() {
                $scope.modalallnetwork.remove();
                $scope.modalcurrentnetwork.remove();
                $scope.modalchannel.remove();
                $scope.modalrecent.remove();
                $scope.modalowning.remove();
            });
        }

    ])
    // close the keyboard after submit //
app.directive('handlePhoneSubmit', function() {
    return function(scope, element, attr) {
        /*var textFields = $(element).children('input[type=text]');
        $(element).submit(function() {
            //console.log('form was submitted');
            textFields.blur();
        });*/
        //console.log(element[0].elements);
    };
});
// video search page of app //
app.controller('VideoSearchCtrl', ['$state', '$scope', '$localStorage', '$sessionStorage', '$location', '$ionicSideMenuDelegate', '$ionicPlatform', '$ionicPopup', '$ionicHistory', 'ItemToSearch',
        function($state, $scope, $localStorage, $sessionStorage, $location, $ionicSideMenuDelegate, $ionicPlatform, $ionicPopup, $ionicHistory, ItemToSearch) {

            $scope.resetSearchedItems = function() {
                $localStorage.$reset();
                $scope.searchItemsStored = [];
                $scope.searchedItemContainer = true;
            }
            if (typeof $localStorage.searchItems !== "undefined") {
                $scope.searchItemsStored = $localStorage.searchItems;
            }
            $scope.SearchItemAgain = function(itemtosearchagain) {
                ItemToSearch.dataItem = itemtosearchagain;
                if ($location.path() == "/app/blog") {
                    $state.reload();
                } else {
                    $state.go('app.blog');
                }
            }
            $scope.SearchItemForm = function() {
                if (this.searchitem) {
                    $scope.searchitem = this.searchitem;
                    $scope.searchedItemContainer = false;

                    if (typeof $localStorage.searchItems === "undefined") {
                        $localStorage.searchItems = [];
                        $localStorage.searchItems.push(this.searchitem);
                        $scope.searchItemsStored = $localStorage.searchItems;
                    } else {
                        if ($localStorage.searchItems.length >= 4) {
                            $localStorage.searchItems.shift();
                        }
                        $localStorage.searchItems.push(this.searchitem);
                        $scope.searchItemsStored = $localStorage.searchItems;
                    }

                    ItemToSearch.dataItem = $scope.searchitem;
                    this.searchitem = "";
                    if ($location.path() == "/app/blog") {
                        $state.reload();
                    } else {
                        $state.go('app.blog');
                    }
                }
            }
            $ionicSideMenuDelegate.canDragContent(true);
            $ionicPlatform.onHardwareBackButton(function() {
                //if ($state.current.name == "app.vsearch") {
                navigator.app.exitApp();
                //}
            }, 100);
            //console.log($state.current.name);
        }
    ])
    // Sign up page of app //
app.controller('SignUpCtrl', ['$state', '$scope', function($state, $scope) {
        // sign up logic here
        $scope.doRegister = function() {
            $state.go('app.login');
        }
    }])
    // Forgot password page of app //
app.controller('ForgotCtrl', ['$scope', function($scope) {
        // forgot password
    }])
    // Forgot password page of app //
app.controller('GalleryCtrl', ['$scope', 'Photos', '$ionicModal', function($scope, Photos, $ionicModal) {

        $scope.items = [];
        $scope.times = 0;
        $scope.postsCompleted = false;
        // load more content function
        $scope.getPosts = function() {
                Photos.getPosts()
                    .success(function(posts) {
                        $scope.items = $scope.items.concat(posts);
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        $scope.times = $scope.times + 1;
                        if ($scope.times >= 4) {
                            $scope.postsCompleted = true;
                        }
                    })
                    .error(function(error) {
                        $scope.items = [];
                    });
            }
            // pull to refresh buttons
        $scope.doRefresh = function() {
                $scope.times = 0;
                $scope.items = [];
                $scope.postsCompleted = false;
                $scope.getPosts();
                $scope.$broadcast('scroll.refreshComplete');
            }
            // modal to show image full screen
        $ionicModal.fromTemplateUrl('templates/image-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });
        $scope.openModal = function() {
            $scope.showNav = true;
            $scope.modal.show();
        };

        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        // show image in popup
        $scope.showImage = function(index) {
                $scope.imageIndex = index;
                $scope.imageSrc = $scope.items[index].image_full;
                $scope.openModal();
            }
            // image navigation // swiping and buttons will also work here
        $scope.imageNavigate = function(dir) {
                if (dir == 'right') {
                    $scope.imageIndex = $scope.imageIndex + 1;
                } else {
                    $scope.imageIndex = $scope.imageIndex - 1;
                }
                //alert(dir);
                if ($scope.items[$scope.imageIndex] === undefined) {
                    $scope.closeModal();
                } else {
                    $scope.imageSrc = $scope.items[$scope.imageIndex].image_full;
                }
            }
            // cleaning modal
        $scope.$on('$stateChangeStart', function() {
            $scope.modal.remove();
        });
    }])
    /*  Videos controller  */
app.controller('VideosCtrl', ['$scope', 'VideoData', '$sce', function($scope, VideoData, $sce) {
        $scope.items = VideoData.items;
        // to work embed in angularjs pages
        $scope.videoEmbed = function(video) {
            return $sce.trustAsResourceUrl(video);
        }
    }])
    /*  Profile page template */
app.controller('ProfileCtrl', ['$scope', function($scope) {
        // just demo profile page
    }])
    /* Blog controller */
app.controller('NewsCtrl', ['$scope', 'Blog', function($scope, Blog) {
        $scope.items = [];
        $scope.times = 0;
        $scope.postsCompleted = false;
        // load more content function
        $scope.getPosts = function() {
                Blog.getPosts()
                    .success(function(posts) {
                        $scope.items = $scope.items.concat(posts);
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        $scope.times = $scope.times + 1;
                        if ($scope.times >= 4) {
                            $scope.postsCompleted = true;
                        }
                    })
                    .error(function(error) {
                        $scope.items = [];
                    });
            }
            // pull to refresh buttons
        $scope.doRefresh = function() {
            $scope.times = 0;
            $scope.items = [];
            $scope.postsCompleted = false;
            $scope.getPosts();
            $scope.$broadcast('scroll.refreshComplete');
        }
    }])
    /* Friends controller */
app.controller('FriendsCtrl', ['$scope', 'Friends', function($scope, Friends) {
    $scope.items = [];
    $scope.times = 0;
    $scope.postsCompleted = false;
    // load more content function
    $scope.getPosts = function() {
            Friends.getFriends()
                .success(function(posts) {
                    $scope.items = $scope.items.concat(posts);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.times = $scope.times + 1;
                    if ($scope.times >= 4) {
                        $scope.postsCompleted = true;
                    }
                })
                .error(function(error) {
                    $scope.items = [];
                });
        }
        // pull to refresh buttons
    $scope.doRefresh = function() {
        $scope.times = 0;
        $scope.items = [];
        $scope.postsCompleted = false;
        $scope.getPosts();
        $scope.$broadcast('scroll.refreshComplete');
    }
}])

/*  Settings Controller */
app.controller('SettingsCtrl', ['$scope', function($scope) {}])
    /*  Settings Controller */
app.controller('SocialProfileCtrl', ['$scope', 'SocialData', function($scope, SocialData) {
        $scope.socials = SocialData.items;
    }])
    /* Features Controller */
app.controller('FeaturesCtrl', ['$scope', 'Features', function($scope, Features) {
        $scope.items = Features.items;
    }])
    /* About us Controller */
app.controller('AboutCtrl', ['$scope', function($scope) {}])
    /* Contact us form page */
app.controller('ContactCtrl', ['$scope', 'ConfigContact', function($scope, ConfigContact) {
        //setting heading here
        $scope.user = [];
        // contact form submit event
        $scope.submitForm = function(isValid) {
            if (isValid) {
                cordova.plugins.email.isAvailable(
                    function(isAvailable) {
                        cordova.plugins.email.open({
                            to: [ConfigContact.EmailId],
                            subject: $scope.user.subject + '-' + ConfigContact.ContactSubject,
                            body: '<h2>' + $scope.user.name + '</h2><br><h3>' + $scope.user.telephone + '</h3><br><h3>' + $scope.user.email + '</h3><br><p>' + $scope.user.details + '</p>',
                            isHtml: true
                        });
                    }
                );
            }
        }
    }])
    // push controller
app.controller('PushCtrl', ['$scope', 'SendPush', function($scope, SendPush) {
    $scope.device_token = $scope.get_device_token();
    $scope.sendNotification = function() {
        SendPush.android($scope.device_token)
            .success(function() {})
            .error(function(error) {});
    }
}]);
// show ad mob here in this page
app.controller('AdmobCtrl', ['$scope', 'ConfigAdmob', function($scope, ConfigAdmob) {
    $scope.showInterstitial = function() {
        if (AdMob) AdMob.showInterstitial();
    }
    document.addEventListener("deviceready", function() {
        if (AdMob) {
            // show admob banner
            if (ConfigAdmob.banner) {
                AdMob.createBanner({
                    adId: ConfigAdmob.banner,
                    position: AdMob.AD_POSITION.BOTTOM_CENTER,
                    autoShow: true
                });
            }
            // preparing admob interstitial ad
            if (ConfigAdmob.interstitial) {
                AdMob.prepareInterstitial({
                    adId: ConfigAdmob.interstitial,
                    autoShow: false
                });
            }
        }
        if (ConfigAdmob.interstitial) {
            $scope.showInterstitial();
        }
    });
}]);
// new items v2.0
// messages list
app.controller('MessagesCtrl', ['$scope', 'Messages', function($scope, Messages) {
    $scope.items = [];
    $scope.postsCompleted = false;
    // load more content function
    $scope.getPosts = function() {
            Messages.getMesages()
                .success(function(posts) {
                    $scope.items = $scope.items.concat(posts);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $scope.postsCompleted = true;
                })
                .error(function(error) {
                    $scope.items = [];
                });
        }
        // pull to refresh buttons
    $scope.doRefresh = function() {
        $scope.items = [];
        $scope.postsCompleted = false;
        $scope.getPosts();
        $scope.$broadcast('scroll.refreshComplete');
    }
}]);
// single message
app.controller('MessageCtrl', ['$scope', 'Messages', '$ionicScrollDelegate', function($scope, Messages, $ionicScrollDelegate) {
    $scope.messages = [];
    $scope.postsCompleted = false;
    // load more content function
    $scope.getPosts = function() {
            Messages.getMessage()
                .success(function(posts) {
                    $scope.messages = $scope.messages.concat(posts);
                    //console.log($scope.messages);
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                    $ionicScrollDelegate.scrollBottom();
                    $scope.postsCompleted = true;
                })
                .error(function(error) {
                    $scope.items = [];
                });
        }
        // pull to refresh buttons
    $scope.doRefresh = function() {
        $scope.messages = [];
        $scope.postsCompleted = false;
        $scope.getPosts();
        $scope.$broadcast('scroll.refreshComplete');
    }
    $scope.addMesage = function() {
        var newMessage = new function() {
            this.message = $scope.datamessage;
            this.from = '2';
            this._id = '12';
            this.title = 'sample';
            this.image = 'http://3.bp.blogspot.com/-bTWNRjookMQ/VYGjnv5nKtI/AAAAAAAAA08/wXshQ9sNDeU/s100-c/blank-792125_1280.jpg';
        }
        $scope.messages = $scope.messages.concat(newMessage);
        $scope.datamessage = "";
        $ionicScrollDelegate.scrollBottom();
    }
}]);
// feed list data sample
app.controller('FeedsListCtrl', ['$scope', '$state', 'Feeds', function($scope, $state, Feeds) {
    $scope.feeds = Feeds.items;
    $scope.showNews = function(index) {
        Feeds.selectedFeed = $scope.feeds[index];
        $state.go('app.feed');
    }
}]);
// single feed posts
app.controller('FeedCtrl', ['$scope', '$state', 'Feeds', function($scope, $state, Feeds) {

    $scope.numPosts = 20;
    $scope.stories = [];
    $scope.feed = Feeds.selectedFeed;
    $scope.showNews = function(index) {
        Feeds.selectedFeed = $scope.feeds[index];
    }
    $scope.loadFeed = function() {
        var feed = new google.feeds.Feed(Feeds.selectedFeed.feed);
        feed.setNumEntries($scope.numPosts);
        var count = 1;
        feed.load(function(result) {
            if (!result.error) {
                $scope.feed = result.feed;
                $scope.$apply();
            }
        });
    }
    $scope.loadFeed();
    $scope.getImage = function(index) {
        var selectedItem = $scope.feed.entries[index];
        var content = selectedItem.content;
        var imgthumb = "";
        a = content.indexOf("<img");
        b = content.indexOf("src=\"", a);
        c = content.indexOf("\"", b + 5);
        d = content.substr(b + 5, c - b - 5);
        if ((a != -1) && (b != -1) && (c != -1) && (d != "")) {
            imgthumb = d;
        }
        if (imgthumb) {
            return imgthumb;
        } else {
            return 'img/photo.png';
        }
    }
    $scope.showFullFeed = function(indexFeed) {
        Feeds.selectedNews = $scope.feed.entries[indexFeed];
        $state.go('app.feedsingle');
    }
}]);
app.controller('FeedSingleCtrl', ['$scope', '$state', 'Feeds', '$sce', function($scope, $state, Feeds, $sce) {
    $scope.feedContent = Feeds.selectedNews;
    $scope.content = $sce.trustAsHtml($scope.feedContent.content);

}]);
