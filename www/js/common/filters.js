app.filter('trusted', ['$sce', function($sce) {
    var div = document.createElement('div');
    return function(text) {
        div.innerHTML = text;
        return $sce.trustAsHtml(div.textContent);
    };
}])
app.filter('htmlToPlaintext', function() {
    return function(text) {
        var rex = /(<([^>]+)>)/ig;
        var txt = document.createElement("textarea");
        txt.innerHTML = decodeURIComponent(escape(text));
        text = txt.value;
        return text.replace(rex, "").substr(0, 56);
    };
})
app.filter('decodeChars', function() {
    return function(text) {
        var rex = /(<([^>]+)>)/ig;
        var txt = document.createElement("textarea");
        txt.innerHTML = decodeURIComponent(escape(text));
        text = txt.value;
        return text.replace(rex, " ");
    };
})
app.filter('strUrlEncode', function() {
    return function(text) {
        return encodeURIComponent(text)
            .replace(/!/g, '%21')
            .replace(/'/g, '%27')
            .replace(/\(/g, '%28')
            .replace(/\)/g, '%29')
            .replace(/\*/g, '%2A')
            .replace(/%20/g, '+');
    }
})
app.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			items.forEach(function(item) {
				var itemMatches = false;

				var keys = Object.keys(props);
				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});
