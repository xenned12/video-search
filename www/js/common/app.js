app.constant("Config", {
  "ApiUrl": "data/feed.json",
  "PhotoUrl": "data/photos.json",
  "CommentUrl": "data/comments.json",
  "FriendsUrl": "data/friends.json",
  "MessagesUrl": "data/messages.json",
  "MessageUrl": "data/message.json",
  "ProductUrl": "data/products.json",
  "WordPress": "http://example.com/",
  "CategoriesListUrl": "http://searchtube.com/parent_categories.json"
})
// config contact
app.constant("ConfigContact", {
  "EmailId": "bennex@wwbn.com, support@wwbn.com",
  "ContactSubject": "Message Received from Searchtube App"
})
// config admon
app.constant("ConfigAdmob", {
  "interstitial": "ca-app-pub-3940256099942544/1033173712",
  "banner": "ca-app-pub-3940256099942544/6300978111"
})
// push notification
app.constant("PushNoti", {
  "senderID": "senderID",
})
